# Blazor

Container image to serve Blazor static content using nginx. Passes ASPNETCORE_ENVIRONMENT through as the Blazor-Environment header.
